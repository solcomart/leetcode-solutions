<?php
// https://leetcode.com/problems/greatest-common-divisor-of-strings/
class Solution {

    /**
     * @param String $str1
     * @param String $str2
     * @return String
     */
    function gcdOfStrings($str1, $str2) {
        $str1_length = strlen($str1);
        $str2_length = strlen($str2);
        $common_divisor = "";

        for ($i = 1; $i <= min($str1_length, $str2_length); $i++) {
            if ($str1_length % $i || $str2_length % $i) continue;
            $divisor = substr($str1, 0, $i);
            $str1_check = str_repeat($divisor, $str1_length/$i);
            if ($str1_check !== $str1) continue;
            $str2_check = str_repeat($divisor, $str2_length/$i);
            if ($str2_check === $str2) $common_divisor = $divisor;
        }

        return $common_divisor;
    }
}

$solution = new Solution();

var_dump($solution->gcdOfStrings("ABCABC", "ABC"));
var_dump($solution->gcdOfStrings("LEET", "CODE"));
