<?php
// https://leetcode.com/problems/two-sum/
class Solution {

    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function twoSum($nums, $target) {
        $cnt = count($nums);
        foreach($nums as $index1 => $num1) {
            for($index2 = ($index1 + 1); $index2 < $cnt; $index2++) {
                if (($num1 + $nums[$index2]) == $target) return [$index1, $index2];
            }
        }
    }
}